/* global saveAs */
/* global require */
(function(w){
    'use strict';
    var mdEditor = require('./editor').mdEditor;
    mdEditor.createEditor('editor');
    var __ = function(id) {
        return w.document.getElementById(id);
    };
    var $input = __('mdEditor-input');
 //   var $preview = __('preview');
    var $save = __('save');
    var $import = __('import');
    var $filename = __('filename').value;
    
    $import.addEventListener('change', function(e) {
        var file = e.target.files[0];
        var reader = new FileReader();

        reader.onload = function(f){
            $input.value = f.target.result;
            __('filename').value = file.name;
            mdEditor.update();
        };
        reader.readAsText(file);
    });
    $save.addEventListener('click', function(){
        var blob = new Blob([$input.value],{type: 'text/plain;charset: utf-8'});
        saveAs(blob, $filename);
    });
})(window);