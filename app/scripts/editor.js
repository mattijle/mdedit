/* global require */
/* global module */
'use strict';
var mdEditor = {
	container: null,
	input: null,
	output: null,
	markdown: require('markdown').markdown,
	update: function update(){
		this.output.innerHTML = this.markdown.toHTML(this.input.value);
	},
	createEditor: function createEditor(containerId){
		var self = this;
		this.container = window.document.getElementById(containerId);
		function createForm(){
			var f = document.createElement('div');
			f.setAttribute('class','mdEditor');
			var ta = document.createElement('textarea');
			ta.setAttribute('id', 'mdEditor-input');
			ta.setAttribute('cols','80');
			ta.setAttribute('rows','20');
			f.appendChild(ta);
			var prevDiv = document.createElement('div');
			var h3 = document.createElement('h3');
			h3.innerHTML = 'Live Preview';
			prevDiv.appendChild(h3);
			var pre = document.createElement('pre');
			pre.setAttribute('id','mdEditor-preview');
			prevDiv.appendChild(pre);
			f.appendChild(prevDiv);
			return f;
		}
		this.container.appendChild(createForm());
		this.input = window.document.getElementById('mdEditor-input');
		this.output = window.document.getElementById('mdEditor-preview');
		this.input.addEventListener('input', function() {
			self.update.call(self);
		});
		this.update();
	}
};
module.exports.mdEditor = mdEditor;