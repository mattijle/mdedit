mdEdit
======

Markdown Editor

Usage:
```JavaScript
mdEditor.createEditor(elementID);
```


Live demo at [http://mattijle.github.io/mdEdit](http://mattijle.github.io/mdEdit)
